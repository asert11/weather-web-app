package org.weather.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.weather.model.Weather;

/**
 * Interfejs do komunikacji z baza danych. *
 */
@Repository("WeatherRepository")
public interface WeatherRepository extends JpaRepository<Weather,Integer> {

    /**
     * Metoda sluzaca do pobrania pogody po id.
     * @param id
     * @return
     */
    Weather findByCityID(int id);

    /**
     * Metoda sluzaca do usuniecia pogody po id.
     * @param id
     * @return
     */
    @Transactional
    Long deleteByCityID(int id);
}
