package org.weather.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.weather.Logger;
import org.weather.Service.WeatherService;
import org.weather.model.Weather;

import java.io.IOException;

@Controller
public class WeatherController {

    private Weather weather;
    private Logger logger;
    @Autowired
    WeatherService weatherService;

    @RequestMapping (value="/", method=RequestMethod.GET)
    public ModelAndView doWeather(@RequestParam(value="city", required=false, defaultValue="Krakow")
                                          String city, ModelAndView model) throws IOException {
        logger = Logger.getInstance();
        String[] strings = null;

        weather = new Weather(city);

        logger.logGetDataFromWebsite("DATA FETCHING FOR " + city);

        if((model = getWeather(weather,city,model)) != null) {
            weatherService.saveWeather(weather);
            model.setViewName("weather");
        }
        else{
            logger.logGetDataFromWebsite("UNSUCCESSFULL DATA FETCHING FOR " + city);
            model.setViewName("weather");
        }
        return model;
    }
    ModelAndView getWeather(Weather weather, String city, ModelAndView model) throws IOException {

        if(weather.getWeather() != null) {
            model.addObject("weatherIcon",weather.getIconID());
            model.addObject("nextWeatherIcon",weather.getNextIconID());
            model.addObject("nextNextWeatherIcon",weather.getNextNextIconID());
            model.addObject("cityName", weather.getCityName());
            model.addObject("description", weather.getDescription());
            model.addObject("temperature", weather.getTemperature());
            model.addObject("nextTemperature", weather.getNextTemperature());
            model.addObject("nextNextTemperature", weather.getNextNextTemperature());
            model.addObject("pressure", weather.getPressure());
            model.addObject("humidity", weather.getHumidity());
            model.addObject("wind", weather.getWind());
            model.addObject("textWeather", weather.toString());
            model.addObject("sunriseTime", weather.getSunriseTime());
            model.addObject("sunsetTime", weather.getSunsetTime());
            model.addObject("time", weather.getTime());
            model.addObject("nextNextDate", weather.getNextNextDate());
            model.addObject("successMessage", "Success message");

            model.addObject("warsawName", weather.getWarsawName());
            model.addObject("warsawiconID", weather.getWarsawiconID());
            model.addObject("warsawTemp", weather.getWarsawTemp());

            model.addObject("poznanName", weather.getPoznanName());
            model.addObject("poznaniconID", weather.getPoznaniconID());
            model.addObject("poznanTemp", weather.getPoznanTemp());

            model.addObject("gdanskName", weather.getGdanskName());
            model.addObject("gdanskiconID", weather.getGdanskiconID());
            model.addObject("gdanskTemp", weather.getGdanskTemp());

            model.addObject("wroclawName", weather.getWroclawName());
            model.addObject("wroclawiconID", weather.getWroclawiconID());
            model.addObject("wroclawTemp", weather.getWroclawTemp());

            model.addObject("zakopaneName", weather.getZakopaneName());
            model.addObject("zakopaneiconID", weather.getZakopaneiconID());
            model.addObject("zakopaneTemp", weather.getZakopaneTemp());

            Weather.HourlyForecastData hf = weather.getHourlyForecast();

            model.addObject("hourForecast", hf.hours);
            model.addObject("temperatureForecast", hf.temperatures);
            model.addObject("iconForecast", hf.icons);

            return model;
        }
        else {
            model.addObject("successMessage", "");
            return model;
        }
    }

}
