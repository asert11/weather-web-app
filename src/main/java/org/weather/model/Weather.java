package org.weather.model;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.HourlyForecast;
import net.aksingh.owmjapis.OpenWeatherMap;
import org.json.JSONException;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *Klasa przechowujaca parametry pogody.
 */
@Entity
@Table(name = "weather")
public class Weather {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int cityID;
    @Column(name = "city_name")
    private String cityName;
    private String description;
    private String temperature;
    private String nextTemperature;
    private String nextNextTemperature;

    private String pressure;
    private String humidity;
    private String wind;

    private String iconID;
    private String nextIconID;
    private String nextNextIconID;
    private Date nextNextDate;

    private String warsawName;
    private String warsawiconID;
    private String warsawTemp;

    private String wroclawName;
    private String wroclawiconID;
    private String wroclawTemp;

    private String poznanName;
    private String poznaniconID;
    private String poznanTemp;

    private String gdanskName;
    private String gdanskiconID;
    private String gdanskTemp;

    private String zakopaneName;
    private String zakopaneiconID;
    private String zakopaneTemp;

    private Date time;

    public String getNextTemperature() {
        return nextTemperature;
    }

    public void setNextTemperature(String nextTemperature) {
        this.nextTemperature = nextTemperature;
    }

    public String getNextNextTemperature() {
        return nextNextTemperature;
    }

    public void setNextNextTemperature(String nextNextTemperature) {
        this.nextNextTemperature = nextNextTemperature;
    }

    public String getNextIconID() {
        return nextIconID;
    }

    public void setNextIconID(String nextDayIconID) {
        this.nextIconID = nextDayIconID;
    }

    public String getNextNextIconID() {
        return nextNextIconID;
    }

    public void setNextNextIconID(String nextNextDayIconID) {
        this.nextNextIconID = nextNextDayIconID;
    }

    private Date sunsetTime;
    private Date sunriseTime;

    public String getTime() {
        String split[] = time.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1] + " | " + split[1] + " " + split[2] + ", " + split[5];
    }
    public String getSunsetTime() {
        String split[] = sunsetTime.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1];
    }
    public void setSunsetTime(Time sunsetTime) {this.sunsetTime = sunsetTime;}
    public String getSunriseTime() {
        String split[] = sunriseTime.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1];
    }
    public void setSunriseTime(Time sunriseTime) {this.sunriseTime = sunriseTime;}

    public int getCityID() {return cityID;}
    public void setCityID(int cityID) {this.cityID = cityID;}
    public String getCityName() {return cityName;}
    public void setCityName(String cityName) {this.cityName = cityName;}
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    public String getTemperature() {return temperature;}
    public void setTemperature(String temperature) {this.temperature = temperature;}
    public String getPressure() {return pressure;}
    public void setPressure(String pressure) {this.pressure = pressure;}
    public String getHumidity() {return humidity;}
    public void setHumidity(String humidity) {this.humidity = humidity;}
    public String getWind() {return wind;}
    public void setWind(String wind) {this.wind = wind;}
    public String getIconID() {return iconID;}
    public void setIconID(String iconID) {this.iconID = iconID;}

    public String getWarsawName() {return this.warsawName;}
    public void setWarsawName(String name) {this.warsawName = name;}
    public String getWarsawiconID() {return this.warsawiconID;}
    public void setWarsawiconID(String id) {this.warsawiconID = id;}
    public String getWarsawTemp() {return this.warsawTemp;}
    public void setWarsawTemp(String temp) {this.warsawTemp = temp;}

    public String getPoznanName() {return this.poznanName;}
    public void setPoznanName(String name) {this.poznanName = name;}
    public String getPoznaniconID() {return this.poznaniconID;}
    public void setPoznaniconID(String id) {this.poznaniconID = id;}
    public String getPoznanTemp() {return this.poznanTemp;}
    public void setPoznanTemp(String temp) {this.poznanTemp = temp;}

    public String getWroclawName() {return this.wroclawName;}
    public void setWroclawName(String name) {this.wroclawName = name;}
    public String getWroclawiconID() {return this.wroclawiconID;}
    public void setWroclawiconID(String id) {this.wroclawiconID = id;}
    public String getWroclawTemp() {return this.wroclawTemp;}
    public void setWroclawTemp(String temp) {this.wroclawTemp = temp;}

    public String getGdanskName() {return this.gdanskName;}
    public void setGdanskName(String name) {this.gdanskName = name;}
    public String getGdanskiconID() {return this.gdanskiconID;}
    public void setGdanskiconID(String id) {this.gdanskiconID = id;}
    public String getGdanskTemp() {return this.gdanskTemp;}
    public void setGdanskTemp(String temp) {this.gdanskTemp = temp;}

    public String getZakopaneName() {return this.zakopaneName;}
    public void setZakopaneName(String name) {this.zakopaneName = name;}
    public String getZakopaneiconID() {return this.zakopaneiconID;}
    public void setZakopaneiconID(String id) {this.zakopaneiconID = id;}
    public String getZakopaneTemp() {return this.zakopaneTemp;}
    public void setZakopaneTemp(String temp) {this.zakopaneTemp = temp;}

    /**
     * Klasa przechowujaca dane pogodowe z podzialem godzinowym.
     */
    public class HourlyForecastData {
        public ArrayList<String> hours;
        public ArrayList<String> temperatures;
        public ArrayList<String> icons;
    }

    /**
     * Metoda zwracajaca godzinowa prognoze pogody.
     *
     * @return HourlyForecastData obiekt zawierający dane pogodowe z podzialem godzinowym.
     * @throws JSONException
     * @throws IOException
     */
    public HourlyForecastData getHourlyForecast() throws JSONException, IOException {
        OpenWeatherMap.Units units = OpenWeatherMap.Units.METRIC;
        OpenWeatherMap wm = new OpenWeatherMap(units, "204e54dfcda152e7756c216dad2dfe27"); // personal OWM API KEY

        CurrentWeather cw = wm.currentWeatherByCityName(cityName);

        if((int)cw.getCityCode() == 0)
            return null;

        HourlyForecastData data = new HourlyForecastData();
        data.hours = new ArrayList<String>();
        data.temperatures = new ArrayList<String>();
        data.icons = new ArrayList<String>();

        try {
            HourlyForecast forecast = wm.hourlyForecastByCityName(cityName);
            DateFormat formatter = new SimpleDateFormat("HH:mm");

            for(int i = 0; i < forecast.getForecastCount(); i++) {

                String date = formatter.format( forecast.getForecastInstance(i).getDateTime());

                String temperature = String.format("%.1f",forecast.getForecastInstance(i).getMainInstance().getTemperature());

                String icon = "";
                if(forecast.getForecastInstance(i).hasWeatherInstance())
                    icon = forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();

                data.hours.add(date);
                data.temperatures.add(temperature);
                data.icons.add(icon);
            }
        }
        catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Metoda zwracajaca date za dwa dni
     * @return
     */
    public String getNextNextDate() {
        String split[] = this.nextNextDate.toString().split(" ");
        return split[0];
    }
    public Weather(){}

    /**
     * Konstruktor
     *
     * @param String cityName
     * @throws JSONException
     * @throws IOException
     */
    public Weather(String cityName) throws JSONException, IOException {
        this.cityName = cityName;
        getWeather();
    }

    /**
     * Metoda pobierajaca dane pogodowe z OWM.
     *
     * @return Weather obiekt zawierajacy dane o pogodzie.
     * @throws JSONException
     * @throws IOException
     */
    public Weather getWeather() throws JSONException, IOException {
        OpenWeatherMap.Units units = OpenWeatherMap.Units.METRIC;
        OpenWeatherMap wm = new OpenWeatherMap(units, "204e54dfcda152e7756c216dad2dfe27"); // personal OWM API KEY

        CurrentWeather cw = wm.currentWeatherByCityName("Warsaw");
        this.warsawName = "Warsaw";
        this.warsawiconID = cw.getWeatherInstance(0).getWeatherIconName();
        float tempC = cw.getMainInstance().getTemperature();
        this.warsawTemp = String.format("%.1f",tempC);

        cw = wm.currentWeatherByCityName("Poznan");
        this.poznanName = "Poznan";
        this.poznaniconID = cw.getWeatherInstance(0).getWeatherIconName();
        tempC = cw.getMainInstance().getTemperature();
        this.poznanTemp = String.format("%.1f",tempC);

        cw = wm.currentWeatherByCityName("Wroclaw");
        this.wroclawName = "Wroclaw";
        this.wroclawiconID = cw.getWeatherInstance(0).getWeatherIconName();
        tempC = cw.getMainInstance().getTemperature();
        this.wroclawTemp = String.format("%.1f",tempC);

        cw = wm.currentWeatherByCityName("Zakopane");
        this.zakopaneName = "Zakopane";
        this.zakopaneiconID = cw.getWeatherInstance(0).getWeatherIconName();
        tempC = cw.getMainInstance().getTemperature();
        this.zakopaneTemp = String.format("%.1f",tempC);

        cw = wm.currentWeatherByCityName("Gdansk");
        this.gdanskName = "Gdansk";
        this.gdanskiconID = cw.getWeatherInstance(0).getWeatherIconName();
        tempC = cw.getMainInstance().getTemperature();
        this.gdanskTemp = String.format("%.1f",tempC);

        cw = wm.currentWeatherByCityName(cityName);

        if((int)cw.getCityCode() == 0)
             return null;

        this.cityID = (int)cw.getCityCode();
        this.description = cw.getWeatherInstance(0).getWeatherDescription();

        tempC = cw.getMainInstance().getTemperature();
        this.temperature = String.format("%.1f",tempC);
        this.pressure = String.valueOf(cw.getMainInstance().getPressure());
        this.humidity = String.valueOf(cw.getMainInstance().getHumidity());
        float kmph = (cw.getWindInstance().getWindSpeed()) * 1.609F;
        this.wind = String.format("%.1f",kmph);
        this.iconID = cw.getWeatherInstance(0).getWeatherIconName();
        this.sunriseTime = cw.getSysInstance().getSunriseTime();
        this.sunsetTime = cw.getSysInstance().getSunsetTime();
        this.time = cw.getDateTime();

        try {
            HourlyForecast forecast = wm.hourlyForecastByCityName(cityName);
            DateFormat formatter = new SimpleDateFormat("HH:mm");

            int i = 0;
            int numEl = 0;

            while (numEl < 2 && i < forecast.getForecastCount()){
                //sformatowanie daty z każdej iteracji do postaci samej godziny ( HH:mm )
                String date2 = formatter.format( forecast.getForecastInstance(i).getDateTime());

                //pobranie temperatury i ikony na następny dzień z godziny 13:00
                if(date2.compareTo("14:00") == 0 || date2.compareTo("13:00") == 0 || date2.compareTo("15:00") == 0){
                    if(numEl == 0){
                        nextTemperature = String.format("%.1f",forecast.getForecastInstance(i).getMainInstance().getTemperature());
                        if(forecast.getForecastInstance(i).hasWeatherInstance())
                            nextIconID = forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();
                        numEl++;
                    }
                    else if(numEl == 1){
                        nextNextTemperature = String.format("%.1f",forecast.getForecastInstance(i).getMainInstance().getTemperature());
                        if(forecast.getForecastInstance(i).hasWeatherInstance())
                            nextNextIconID =  forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();
                        nextNextDate = forecast.getForecastInstance(i).getDateTime();
                        numEl++;
                    }
                }
                i++;
            }
        }
        catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return this;
    }
    @Override
    public String toString() {
        return
                "ID: " + cityID + "\n" +
                        "City Name: " + cityName + "\n" +
                        "Description: " + description + "\n" +
                        "Temperature: " + temperature + " C" + "\n" +
                        "Pressure: " + pressure + " mbar" + "\n" +
                        "Humidity: " + humidity + " %" + "\n" +
                        "Wind: " + wind + " kmph" + "\n" +
                        "Sunrise Time: " + getSunriseTime() + "\n" +
                        "Sunset Time:  " + getSunsetTime() + "\n" +
                        "Icon Name: " + iconID;
    }

}
