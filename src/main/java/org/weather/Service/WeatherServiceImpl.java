package org.weather.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.weather.Repository.WeatherRepository;
import org.weather.model.Weather;

@Service("weatherService")
public class WeatherServiceImpl implements WeatherService{


    @Autowired
    private WeatherRepository weatherRepository;

    @Override
    public Weather findWeatherByCityID(int id) {
        return weatherRepository.findByCityID(id);
    }

    @Override
    public void saveWeather(Weather weather) {
        weatherRepository.save(weather);
    }
}
