package org.weather.Service;

import org.weather.model.Weather;


public interface WeatherService {
    public Weather findWeatherByCityID(int id);
    public void saveWeather(Weather weather);
}


