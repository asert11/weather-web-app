<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--<link href="${pageContext.request.contextPath}resources/css/weather.css" type="text/css" rel="stylesheet">--%>
        <title>Weather App</title>
        <!-- bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!-- fontawesome -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- weather icons -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css">
    <link href="../css/style.css" rel="stylesheet" type="text/css">
<style>
    body {
        z-index: -5;
        min-height: 100vh;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        margin: 0 auto;
        color: white;
    }
    /* background color on top of bg image*/
    .hero {

        min-height: 100vh;
        min-width: 100%;
        top: 0;
        bottom: 0;
        background-color: #778899;
    }
    /* navbar */
    .navbar {
        background-color: rgb(69, 106, 171);
    }
    .navbar a {
        color: white;
        font-size: 25px;
    }
    /* weather section */
    .weather {
        border:1px solid white;
        margin-top: 5rem;
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 20px;
        color: white;
    }
    .weather-head {
        height: 50%;
    }
    #icon-desc {
        font-size: 5rem;
        padding-top: 2rem;
    }
    .humidity, .wind, .pressure {
        border-top: 1px solid white;
        border-bottom: 1px solid white;
        font-size: 1.5rem;
    }
    #description {
        font-size: 2rem;
    }
    #temperature {
        font-size: 4rem;
        padding-top: 2rem;
    }
    #icon-thermometer {
        font-size: 6rem;
    }
    #humidity, #wind, #pressure {
        font-size: 2rem;
        padding-top: 1rem;
        padding-bottom: 1rem;
    }
    #tomorrow {
        border-top: 1px solid white;
        border-right: 1px solid white;
        border-top-right-radius: 1rem;
        font-size: 5rem;
    }
    #nextDay {
        border-top: 1px solid white;
        border-top-left-radius: 1rem;
        font-size: 5rem;
    }
    #tomorrowH1 {
        font-size: 1rem;
        text-align: left;
    }
    #tommorowDescription {
        font-size: 1rem;
    }
    #tomorrowDesc {
        font-size: 2rem;
    }
    #tomorrowIcon-desc {
        font-size: 3rem;
    }
    #bigCity {
        border:1px solid white;
        margin-top: 8rem;
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 20px;
        color: white;
        height: 80%;
    }
    .cities {
        border-bottom: 1px solid white;
    }
    .bigCitydesc {
        margin-top: 1rem;
        margin-left: 2rem;
        font-size: 2rem;
    }
    .bigCityicon {
        margin-top: 1rem;
        font-size: 3rem;
    }
    .bigCitytemp {
        margin-top: 0.75rem;
        font-size: 2rem;
    }
    /* openweathermap */
    .owm-01d:before {content: "\f00d";}
    .owm-02d:before {content: "\f002";}
    .owm-03d:before {content: "\f041";}
    .owm-04d:before {content: "\f013";}
    .owm-09d:before {content: "\f01a";}
    .owm-10d:before {content: "\f008";}
    .owm-11d:before {content: "\f01e";}/*f010*/
    .owm-13d:before {content: "\f01b";}
    .owm-50d:before {content: "\f003";}
    .owm-01n:before {content: "\f02e";}
    .owm-02n:before {content: "\f086";}
    .owm-03n:before {content: "\f041";}
    .owm-04n:before {content: "\f013";}
    .owm-09n:before {content: "\f01a";}
    .owm-10n:before {content: "\f028";}
    .owm-11n:before {content: "\f01e";}/*f02d*/
    .owm-13n:before {content: "\f01b";}
    .owm-50n:before {content: "\f04a";}
</style>
</head>
<body>

<div class="hero container">
    <!-- navbar -->
    <nav class="navbar row">
        <a class="navbar-brand" href="#">
            <i class="fa fa-sun-o" aria-hidden="true"></i>
            <strong>Weather</strong>Service
        </a>
        <form class="form-inline" method="get" commandName="weather" id="forma">
            <div class="form-group mx-sm-3 mb-2">
            <label for="city" class="sr-only">Password</label>
            <input class="form-control" type="text" name="city" id="city" placeholder="Search city">
            </div>
            <input class="btn btn-primary mb-2" type="submit" value="Show weather" />
        </form>
        <span id="date" class="lead">${time}</span>
    </nav>
    <!-- weather section  -->
        <div class="row justify-content-around">
            <div id="cont" class="col-6 weather">
                <!-- weather header section -->
                <div class="weather-head">
                    <h1 id="location" class="text-center display-4">${cityName}</h1>
                    <div class="row">
                        <div id="description" class="description col-6 text-center">
                            <i id="icon-desc" class="wi owm-${weatherIcon}"></i>
                            <p class="desc">${description}</p>
                        </div>
                        <div id="temperature" class="col-6 text-center">${temperature}&#8451; <i id="icon-thermometer" class="wi wi-thermometer"></i>
                        </div>
                    </div>
                    <!-- weather body header -->
                    <div class="weather-body">
                        <div class="row">
                            <div class="humidity col-4 text-center">
                                <i class="wi wi-raindrop"></i><span> Humidity</span>
                            </div>
                            <div class="wind col-4 text-center">
                                <i class="wi wi-strong-wind"></i><span> Wind Speed</span>
                            </div>
                            <div class="pressure col-4 text-center">
                                <i class="fa fa-eye"></i><span> Pressure</span>
                            </div>
                        </div>
                        <!-- weather body data -->
                        <div class="row">
                            <div id="humidity" class="humidity-data col-4 text-center">
                                ${humidity} %
                            </div>
                            <div id="wind" class="wind-data col-4 text-center">
                                ${wind} km/h
                            </div>
                            <div id="pressure" class="degree-data col-4 text-center">
                                ${pressure} mbar.
                            </div>
                        </div>
                        <div class="row">
                            <div id="tomorrow" class="humidity-data col-6 text-center">
                                <h1 id="tomorrowH1">Tommorow</h1>
                                <div id="tommorowDescription" class="description col-12 text-center">
                                    <i id="tomorrowIcon-desc" class="wi owm-${nextWeatherIcon}"></i>
                                    <p id="tomorrowDesc">${nextTemperature}&#8451;</p>
                                </div>
                            </div>
                            <div id="nextDay" class="wind-data col-6 text-center">
                                <h1 id="tomorrowH1">${nextNextDate}</h1>
                                <div id="tommorowDescription" class="description col-12 text-center">
                                    <i id="tomorrowIcon-desc" class="wi owm-${nextNextWeatherIcon}"></i>
                                    <p id="tomorrowDesc">${nextNextTemperature}&#8451;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="bigCity" class="col-4">
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${warsawName}</p>
                    <i class="bigCityicon wi owm-${warsawiconID}"></i>
                    <p class="bigCitytemp">${warsawTemp}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${poznanName}</p>
                    <i class="bigCityicon wi owm-${poznaniconID}"></i>
                    <p class="bigCitytemp">${poznanTemp}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${wroclawName}</p>
                    <i class="bigCityicon wi owm-${wroclawiconID}"></i>
                    <p class="bigCitytemp">${wroclawTemp}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${gdanskName}</p>
                    <i class="bigCityicon wi owm-${gdanskiconID}"></i>
                    <p class="bigCitytemp">${gdanskTemp}&#8451;</p>
                </div>
                <div class="row justify-content-around">
                    <p class="bigCitydesc col-6">${zakopaneName}</p>
                    <i class="bigCityicon wi owm-${zakopaneiconID}"></i>
                    <p class="bigCitytemp">${zakopaneTemp}&#8451;</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-around">
            <div id="bigCity" class="col-4">
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(0)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(0)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(0)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(1)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(1)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(1)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(2)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(2)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(2)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(3)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(3)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(3)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(4)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(4)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(4)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(5)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(5)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(5)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(6)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(6)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(6)}&#8451;</p>
                </div>
                <div class="row cities justify-content-around">
                    <p class="bigCitydesc col-6">${hourForecast.get(7)}</p>
                    <i class="bigCityicon wi owm-${iconForecast.get(7)}"></i>
                    <p class="bigCitytemp">${temperatureForecast.get(7)}&#8451;</p>
                </div>
            </div>
        </div>
</div>
</body>
</html>